{
  "@context": "https://doi.org/10.5063/schema/codemeta-2.0",
  "@type": "SoftwareSourceCode",
  "identifier": "SOMbrero",
  "description": "The stochastic (also called on-line) version of the Self-Organising Map (SOM) algorithm is provided. Different versions of the algorithm are implemented, for numeric and relational data and for contingency tables as described, respectively, in Kohonen (2001) <isbn:3-540-67921-9>, Olteanu & Villa-Vialaneix (2005) <doi:10.1016/j.neucom.2013.11.047> and Cottrell et al (2004) <doi:10.1016/j.neunet.2004.07.010>. The package also contains many plotting features (to help the user interpret the results), can handle (and impute) missing values and is delivered with a graphical user interface based on 'shiny'.",
  "name": "SOMbrero: SOM Bound to Realize Euclidean and Relational Outputs",
  "codeRepository": "https://github.com/tuxette/SOMbrero",
  "issueTracker": "https://github.com/tuxette/SOMbrero/issues",
  "license": "https://spdx.org/licenses/GPL-2.0",
  "version": "1.4.2",
  "programmingLanguage": {
    "@type": "ComputerLanguage",
    "name": "R",
    "url": "https://r-project.org"
  },
  "runtimePlatform": "R version 4.4.2 (2024-10-31)",
  "provider": {
    "@id": "https://cran.r-project.org",
    "@type": "Organization",
    "name": "Comprehensive R Archive Network (CRAN)",
    "url": "https://cran.r-project.org"
  },
  "author": [
    {
      "@type": "Person",
      "givenName": "Nathalie",
      "familyName": "Vialaneix",
      "email": "nathalie.vialaneix@inrae.fr",
      "@id": "https://orcid.org/0000-0003-1156-0639"
    },
    {
      "@type": "Person",
      "givenName": "Elise",
      "familyName": "Maigne",
      "email": "elise.maigne@inrae.fr"
    },
    {
      "@type": "Person",
      "givenName": "Jerome",
      "familyName": "Mariette",
      "email": "jerome.mariette@inrae.fr"
    },
    {
      "@type": "Person",
      "givenName": "Madalina",
      "familyName": "Olteanu",
      "email": "olteanu@ceremade.dauphine.fr"
    },
    {
      "@type": "Person",
      "givenName": "Fabrice",
      "familyName": "Rossi",
      "email": "fabrice.rossi@apiacoa.org"
    }
  ],
  "contributor": [
    {
      "@type": "Person",
      "givenName": "Laura",
      "familyName": "Bendhaiba",
      "email": "laurabendhaiba@gmail.com"
    },
    {
      "@type": "Person",
      "givenName": "Julien",
      "familyName": "Boelaert",
      "email": "julien.boelaert@gmail.com"
    }
  ],
  "maintainer": [
    {
      "@type": "Person",
      "givenName": "Nathalie",
      "familyName": "Vialaneix",
      "email": "nathalie.vialaneix@inrae.fr",
      "@id": "https://orcid.org/0000-0003-1156-0639"
    }
  ],
  "softwareSuggestions": [
    {
      "@type": "SoftwareApplication",
      "identifier": "testthat",
      "name": "testthat",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=testthat"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "rmarkdown",
      "name": "rmarkdown",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=rmarkdown"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "knitr",
      "name": "knitr",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=knitr"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "hexbin",
      "name": "hexbin",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=hexbin"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "shinycssloaders",
      "name": "shinycssloaders",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=shinycssloaders"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "shinyBS",
      "name": "shinyBS",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=shinyBS"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "shinyjs",
      "name": "shinyjs",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=shinyjs"
    },
    {
      "@type": "SoftwareApplication",
      "identifier": "shinyjqui",
      "name": "shinyjqui",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=shinyjqui"
    }
  ],
  "softwareRequirements": {
    "1": {
      "@type": "SoftwareApplication",
      "identifier": "R",
      "name": "R",
      "version": ">= 3.1.0"
    },
    "2": {
      "@type": "SoftwareApplication",
      "identifier": "igraph",
      "name": "igraph",
      "version": ">= 1.0",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=igraph"
    },
    "3": {
      "@type": "SoftwareApplication",
      "identifier": "markdown",
      "name": "markdown",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=markdown"
    },
    "4": {
      "@type": "SoftwareApplication",
      "identifier": "scatterplot3d",
      "name": "scatterplot3d",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=scatterplot3d"
    },
    "5": {
      "@type": "SoftwareApplication",
      "identifier": "shiny",
      "name": "shiny",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=shiny"
    },
    "6": {
      "@type": "SoftwareApplication",
      "identifier": "grDevices",
      "name": "grDevices"
    },
    "7": {
      "@type": "SoftwareApplication",
      "identifier": "graphics",
      "name": "graphics"
    },
    "8": {
      "@type": "SoftwareApplication",
      "identifier": "stats",
      "name": "stats"
    },
    "9": {
      "@type": "SoftwareApplication",
      "identifier": "ggplot2",
      "name": "ggplot2",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=ggplot2"
    },
    "10": {
      "@type": "SoftwareApplication",
      "identifier": "ggwordcloud",
      "name": "ggwordcloud",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=ggwordcloud"
    },
    "11": {
      "@type": "SoftwareApplication",
      "identifier": "metR",
      "name": "metR",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=metR"
    },
    "12": {
      "@type": "SoftwareApplication",
      "identifier": "interp",
      "name": "interp",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=interp"
    },
    "13": {
      "@type": "SoftwareApplication",
      "identifier": "rlang",
      "name": "rlang",
      "provider": {
        "@id": "https://cran.r-project.org",
        "@type": "Organization",
        "name": "Comprehensive R Archive Network (CRAN)",
        "url": "https://cran.r-project.org"
      },
      "sameAs": "https://CRAN.R-project.org/package=rlang"
    },
    "SystemRequirements": null
  },
  "fileSize": "634.998KB",
  "citation": [
    {
      "@type": "ScholarlyArticle",
      "datePublished": "2015",
      "author": [
        {
          "@type": "Person",
          "givenName": "Madalina",
          "familyName": "Olteanu"
        },
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Villa-Vialaneix"
        }
      ],
      "name": "On-line relational and multiple relational SOM",
      "identifier": "10.1016/j.neucom.2013.11.047",
      "pagination": "15--30",
      "@id": "https://doi.org/10.1016/j.neucom.2013.11.047",
      "sameAs": "https://doi.org/10.1016/j.neucom.2013.11.047",
      "isPartOf": {
        "@type": "PublicationIssue",
        "datePublished": "2015",
        "isPartOf": {
          "@type": ["PublicationVolume", "Periodical"],
          "volumeNumber": "147",
          "name": "Neurocomputing"
        }
      }
    },
    {
      "datePublished": "2017",
      "author": [
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Villa-Vialaneix"
        }
      ],
      "name": "Stochastic self-organizing map variants with the R package SOMbrero",
      "pagination": "1--7"
    },
    {
      "datePublished": "2012",
      "author": [
        {
          "@type": "Person",
          "givenName": "Madalina",
          "familyName": "Olteanu"
        },
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Villa-Vialaneix"
        },
        {
          "@type": "Person",
          "givenName": "Marie",
          "familyName": "Cottrell"
        }
      ],
      "name": "On-line relational SOM for dissimilarity data",
      "pagination": "13--22"
    },
    {
      "@type": "ScholarlyArticle",
      "datePublished": "2015b",
      "author": [
        {
          "@type": "Person",
          "givenName": "Madalina",
          "familyName": "Olteanu"
        },
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Villa-Vialaneix"
        }
      ],
      "name": "Using SOMbrero for clustering and visualizing graphs",
      "pagination": "95--119",
      "isPartOf": {
        "@type": "PublicationIssue",
        "datePublished": "2015b",
        "isPartOf": {
          "@type": ["PublicationVolume", "Periodical"],
          "volumeNumber": "156",
          "name": "Journal de la Societe Francaise de Statistique"
        }
      }
    },
    {
      "datePublished": "2017",
      "author": [
        {
          "@type": "Person",
          "givenName": "Jerome",
          "familyName": "Mariette"
        },
        {
          "@type": "Person",
          "givenName": "Fabrice",
          "familyName": "Rossi"
        },
        {
          "@type": "Person",
          "givenName": "Madalina",
          "familyName": "Olteanu"
        },
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Villa-Vialaneix"
        }
      ],
      "name": "Accelerating stochastic kernel SOM",
      "pagination": "269--274"
    },
    {
      "@type": "SoftwareSourceCode",
      "datePublished": "2024",
      "author": [
        {
          "@type": "Person",
          "givenName": "Nathalie",
          "familyName": "Vialaneix",
          "email": "nathalie.vialaneix@inrae.fr",
          "@id": "https://orcid.org/0000-0003-1156-0639"
        },
        {
          "@type": "Person",
          "givenName": "Elise",
          "familyName": "Maigne",
          "email": "elise.maigne@inrae.fr"
        },
        {
          "@type": "Person",
          "givenName": "Jerome",
          "familyName": "Mariette",
          "email": "jerome.mariette@inrae.fr"
        },
        {
          "@type": "Person",
          "givenName": "Madalina",
          "familyName": "Olteanu",
          "email": "olteanu@ceremade.dauphine.fr"
        },
        {
          "@type": "Person",
          "givenName": "Fabrice",
          "familyName": "Rossi",
          "email": "fabrice.rossi@apiacoa.org"
        }
      ],
      "name": "SOMbrero: SOM Bound to Realize Euclidean and Relational Outputs",
      "url": "https://CRAN.R-project.org/package=SOMbrero",
      "description": "R package version 1.4-2 --- For new features, see the 'NEWS'"
    }
  ],
  "relatedLink": "http://sombrero.clementine.wf/",
  "releaseNotes": "https://github.com/tuxette/SOMbrero/blob/master/NEWS.md",
  "readme": "https://github.com/tuxette/SOMbrero/blob/master/README.md"
}
